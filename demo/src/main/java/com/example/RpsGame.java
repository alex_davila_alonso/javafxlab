package com.example;

import java.util.Random;

public class RpsGame {
    private int wins;
    private int ties;
    private int losses;
    private Random num;
    public RpsGame() {
        this.wins = 0;
        this.ties = 0;
        this.losses = 0;
        this.num = new Random();
    }
    

    

    public int getWins() {
        return wins;
    }
    public int getTies() {
        return ties;
    }
    public int getLosses() {
        return losses;
    }
    public String playRound(String choice){
        String computerChoice = "";
        int randNum = this.num.nextInt(3)+1;
        switch(randNum){
            case 1:
                computerChoice = "rock";
                break;
            case 2:
                computerChoice = "paper";
                break;
            case 3:
                computerChoice = "scissor";
                break;
        }
        String outcome = "";
        if(choice.equals(computerChoice)){
            this.ties++;
            outcome = "It is a tie, computer and Player picked " + choice;
        }
        else if(computerChoice.equals("scissor") && choice.equals("paper")){
            this.losses++;
            outcome =  "Computer plays "+computerChoice+" and wins";
        }
        else if(computerChoice.equals("rock") && choice.equals("scissor")){
            this.losses++;
            outcome = "Computer plays "+computerChoice+" and wins";
        }
        else if(computerChoice.equals("paper") && choice.equals("rock")){
            this.losses++;
            outcome = "Computer plays "+computerChoice+" and wins";
        }
        else if(computerChoice.equals("rock") && choice.equals("paper")){
            this.wins++;
            outcome = "You played "+choice+" and won";
        }
        else if(computerChoice.equals("paper") && choice.equals("scissor")){
            this.wins++;
            outcome = "You played "+choice+" and won";
        }
        else if(computerChoice.equals("scissor") && choice.equals("rock")){
            this.wins++;
            outcome = "You played "+choice+" and won";
        }
        else{
            outcome = "please enter a correct choice";
        }
        
        return outcome;
    }

   
}
