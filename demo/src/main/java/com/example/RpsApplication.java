package com.example;

import javafx.application.Application;
import javafx.application.*;
import javafx.event.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Text;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {
    private RpsGame game;

    public void start(Stage stage){
        Group root = new Group();

        Scene scene = new Scene(root,650,300);
        scene.setFill(Color.BLACK);

        stage.setTitle("Rock Paper Scissors");
        stage.setScene(scene);

        Button rock = new Button("rock");
        Button scissor = new Button("scissor");
        Button paper = new Button("paper");

        HBox buttons = new HBox();
        HBox textFields = new HBox();
        VBox overall = new VBox();

        TextField message = new TextField("Welcome");
        TextField wins = new TextField("wins: 0");
        TextField losses = new TextField("losses: 0");
        TextField ties = new TextField("ties: 0");

        rock.setOnAction(new RpsChoice(message, wins, losses, ties, "rock", game));
        paper.setOnAction(new RpsChoice(message, wins, losses, ties, "paper", game));
        scissor.setOnAction(new RpsChoice(message, wins, losses, ties, "scissor", game));

        message.setPrefWidth(200);
    
        buttons.getChildren().addAll(rock,scissor,paper);
        textFields.getChildren().addAll(message,wins,losses,ties);
        overall.getChildren().addAll(buttons,textFields);
        root.getChildren().addAll(overall);

        stage.show();
    }
    public static void main(String[] args) {
        Application.launch(args);
    }
}
